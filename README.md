# liquibase demo

Database testing requirements vary across different environments. In terms of the database, teams may wish to use different types of databases and different data sets to satisfy their test plans. We will provide an example of how a team could configure Liquibase to use a different database and data set across multiple environments. 
There's a Doc in this [link](https://aiacom-my.sharepoint.com/:w:/g/personal/andy-d_yang_aia_com/ERr0OyHOPWBMp6y9VPfj9hMBefEaOL7AJjZJvb6e16oMjA).


## Description
Our sample code uses Maven profiles to distinguish liquibase.properties files used in different environments. And we use springboot application profiles to distinguish the config in different environments. 



## TEST Run the Sample code

### local development environment
We use local.liquibase.properties file as our local development environment and we use an H2 embeded database so that there's no need to setup database in local development environment. 

You can test your setup with following command with local profile:
```
mvn liquibase:status -Plocal
```

To run our sample application in local dev environment, we will use the following command:
```
mvn clean spring-boot:run -Dspring-boot.run.profiles=local
```

### TEST environment
We use TEST.liquibase.properties file as our test environment and we use mysql database in test environment. 

You can test your setup with following command with TEST profile:
```
mvn liquibase:status -PTEST
```

To run our sample application in test environment, we will use the following command:
```
mvn clean spring-boot:run -Dspring-boot.run.profiles=TEST
```



## Package the sample application and run it with environment profile

We use mvn to package the application:
```
mvn clean package -Dmaven.test.skip=true
```
There's a JAR file generated in the target folder. 

Then we can run the JAR file with environment profile:
```
java -jar -Dspring.profiles.active=local target/SalesManager-0.0.1-SNAPSHOT.jar
```
Through the -Dspring.profiles.active system property, we can activate different configuration with different profile, to run the application in different environment.
