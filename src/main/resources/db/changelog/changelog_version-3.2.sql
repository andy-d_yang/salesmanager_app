--liquibase formatted sql

--changeset SteveZ:1582471835439-1
CREATE TABLE SALES (ID BIGINT AUTO_INCREMENT PRIMARY KEY, ITEM VARCHAR(255), QUANTITY BIGINT, AMOUNT FLOAT);
--rollback DROP TABLE SALES;

--changeset JohnD:15824-2 context:dev
INSERT INTO SALES (ITEM, QUANTITY, AMOUNT) VALUES ('Dev - Leather sofa By Michellotti', 4, 5026.63);
--rollback DELETE FROM SALES WHERE ITEM='Dev - Leather sofa By Michellotti';

--changeset JohnD:1582471835439-6 context:test
INSERT INTO SALES (ITEM, QUANTITY, AMOUNT) VALUES ('Test - Round coffee table By Michellotti', 1, 800.23);
--rollback DELETE FROM SALES WHERE ITEM='Test - Round coffee table By Michellotti';
